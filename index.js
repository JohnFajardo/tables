document.addEventListener('DOMContentLoaded', () => {
    let table = document.getElementById("ems-table");

    let data = {
        object1: {
            value: ["value x"],
            action: "action a"
        },
        object2: {
            value: ["value 1", "value 2", "value 3"],
            action: "action b"
        },
        object3: {
            value: ["value y"],
            action: "action c"
        },
        object4: {
            value: ["value z"],
            action: "action d"
        },
    };

    Object.keys(data).forEach((element, index) => {
        let row = document.createElement('tr');
        let indexCell = document.createElement('td');
        indexCell.innerHTML = index;
        row.appendChild(indexCell);

        Object.values(data[element]).forEach((element) => {
            let cell = document.createElement('td');
            if(Array.isArray(element) && element.length > 1) {
                element.forEach((value) => {
                    let paragraph = document.createElement('p');
                    paragraph.innerText = value
                    cell.appendChild(paragraph);
                });
            }
            if(Array.isArray(element) && element.length === 1) {
                element.forEach((value) => {
                    cell.innerHTML += value;
                });
            }
            if(typeof element === "string") {
                cell.innerHTML = element;
            }
            row.appendChild(cell);
        });
        table.appendChild(row);
    });
});
